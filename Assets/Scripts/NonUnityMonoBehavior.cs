using UnityEngine;


public abstract class NonUnityMonoBehavior
{
    #region MonoBehavior Properties

    public virtual GameObject gameObject { get; set; }

    #endregion // MonoBehavior Fields

    #region MonoBehavior Methods

    public virtual void Awake()
    {
    }

    public virtual void Start()
    {
    }

    #region Update Methods

    public virtual void Update()
    {
    }

    public virtual void LateUpdate()
    {
    }

    public virtual void FixedUpdate()
    {
    }

    #endregion // Update Methods

    public virtual void Reset()
    {
    }

    #region On... Methods

    #region OnMouse... Methods

    public virtual void OnMouseEnter()
    {
    }

    public virtual void OnMouseExit()
    {
    }

    public virtual void OnMouseOver()
    {
    }

    public virtual void OnMouseDown()
    {
    }

    public virtual void OnMouseUp()
    {
    }

    public virtual void OnMouseUpAsButton()
    {
    }

    public virtual void OnMouseDrag()
    {
    }

    #endregion // OnMouse... Methods

    #endregion // On... Methods

    #endregion // MonoBehavior Methods
}
