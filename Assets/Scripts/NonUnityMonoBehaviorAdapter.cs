using UnityEngine;
using System;


public sealed class NonUnityMonoBehaviorAdapter : MonoBehaviour 
{
    public string NonUnityMonoBehaviorName; // This is mostly for assigning NonUnityMonoBehaviours via the Inspector
    private NonUnityMonoBehavior _nonUnityMonoBehavior;
    private bool _isInitialized;


    public void Init(NonUnityMonoBehavior nonUnityMonoBehavior)
    {
        if (nonUnityMonoBehavior == null) throw new ArgumentNullException("nonUnityMonoBehavior");

        NonUnityMonoBehaviorName = nonUnityMonoBehavior.GetType().Name;
        _nonUnityMonoBehavior = nonUnityMonoBehavior;
        _nonUnityMonoBehavior.gameObject = gameObject;

        _isInitialized = true;
    }

    #region MonoBehavior Methods
    
    private void Awake()
    {
        if (_isInitialized || string.IsNullOrEmpty(NonUnityMonoBehaviorName)) return;

        // We can only get here if we add the Adapter via inspector, not at runtime

        var type = Type.GetType(NonUnityMonoBehaviorName);
        if (type != null)
        {
            _nonUnityMonoBehavior = Activator.CreateInstance(type) as NonUnityMonoBehavior;

            if (_nonUnityMonoBehavior == null) throw new NullReferenceException("_nonUnityMonoBehavior is null, can't Awake");

            _nonUnityMonoBehavior.gameObject = gameObject;

            _isInitialized = true;

            _nonUnityMonoBehavior.Awake();
        }
    }

    private void Start()
    {
        if (_nonUnityMonoBehavior == null) throw new NullReferenceException("_nonUnityMonoBehavior is null, can't Start");

        _nonUnityMonoBehavior.Start();
    }

    #region Update Methods

    private void Update()
    {
        if (_nonUnityMonoBehavior == null) throw new NullReferenceException("_nonUnityMonoBehavior is null, can't Update");

        _nonUnityMonoBehavior.Update();
    }

    private void LateUpdate()
    {
        if (_nonUnityMonoBehavior == null) throw new NullReferenceException("_nonUnityMonoBehavior is null, can't LateUpdate");

        _nonUnityMonoBehavior.LateUpdate();
    }

    private void FixedUpdate()
    {
        if (_nonUnityMonoBehavior == null) throw new NullReferenceException("_nonUnityMonoBehavior is null, can't FixedUpdate");

        _nonUnityMonoBehavior.FixedUpdate();
    }

    #endregion // Update Methods

    public void Reset()
    {
        if (_nonUnityMonoBehavior == null) return;// throw new NullReferenceException("_nonUnityMonoBehavior is null, can't Reset");

        _nonUnityMonoBehavior.Reset();
    }

    #region On... Methods

    #region OnMouse... Methods

    private void OnMouseEnter()
    {
        if (_nonUnityMonoBehavior == null) throw new NullReferenceException("_nonUnityMonoBehavior is null, can't OnMouseEnter");

        _nonUnityMonoBehavior.OnMouseEnter();
    }

    private void OnMouseExit()
    {
        if (_nonUnityMonoBehavior == null) throw new NullReferenceException("_nonUnityMonoBehavior is null, can't OnMouseExit");

        _nonUnityMonoBehavior.OnMouseExit();
    }

    private void OnMouseOver()
    {
        if (_nonUnityMonoBehavior == null) throw new NullReferenceException("_nonUnityMonoBehavior is null, can't OnMouseOver");

        _nonUnityMonoBehavior.OnMouseOver();
    }

    private void OnMouseDown()
    {
        if (_nonUnityMonoBehavior == null) throw new NullReferenceException("_nonUnityMonoBehavior is null, can't OnMouseDown");

        _nonUnityMonoBehavior.OnMouseDown();
    }

    private void OnMouseUp()
    {
        if (_nonUnityMonoBehavior == null) throw new NullReferenceException("_nonUnityMonoBehavior is null, can't OnMouseUp");

        _nonUnityMonoBehavior.OnMouseUp();
    }

    private void OnMouseUpAsButton()
    {
        if (_nonUnityMonoBehavior == null) throw new NullReferenceException("_nonUnityMonoBehavior is null, can't OnMouseUpAsButton");

        _nonUnityMonoBehavior.OnMouseUpAsButton();
    }

    private void OnMouseDrag()
    {
        if (_nonUnityMonoBehavior == null) throw new NullReferenceException("_nonUnityMonoBehavior is null, can't OnMouseDrag");

        _nonUnityMonoBehavior.OnMouseDrag();
    }

    #endregion // OnMouse... Methods

    #endregion // On... Methods

    #endregion // MonoBehavior Methods
}
