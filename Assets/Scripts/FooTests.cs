using NUnit.Framework;

[TestFixture]
public class FooTest
{
	// Hier wird Foo getestet
    [Test]
    public void AddFooToFoo_ShouldReturnFoo()
    {
        DerivedFromNon test = new DerivedFromNon();

        test.Update();
        test.Update();

        Assert.AreEqual(1, 1);
    }
}