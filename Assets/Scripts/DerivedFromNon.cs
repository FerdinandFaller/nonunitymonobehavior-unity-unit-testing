using UnityEngine;


public class DerivedFromNon : NonUnityMonoBehavior 
{
    public override void Awake() 
    {
        Debug.Log("This works great!");

        var adapter = gameObject.AddComponent<NonUnityMonoBehaviorAdapter>();
        adapter.Init(new DerivedFromNon2());
	}
}
